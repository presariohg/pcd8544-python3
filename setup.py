from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup( name              =  'pcd8544',
       version           =  '0.1.2',
       description       =  'A Python 3 library for PCD8544 LCD, best suited for Raspberry Pi',
       long_description  =  long_description,
       long_description_content_type = "text/markdown",
       url               =  'https://gitlab.com/presariohg/pcd8544-python3',
       author            =  'Tam Nguyen',
       author_email      =  'nguyenductam_t61@hus.edu.vn',
       license           =  'MIT',
       packages          =  find_packages(),
       install_requires  =  [
                              'wiringpi',
                              'spidev'
                            ],
       classifiers       =  [
                              "Programming Language :: Python :: 3",
                              "License :: OSI Approved :: MIT License",
                              "Operating System :: OS Independent",
                            ],
)